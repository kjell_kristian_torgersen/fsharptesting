﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.
open System.IO
open System.Text.RegularExpressions
open System.Windows.Forms
open System.Drawing
open OxyPlot.WindowsForms
open OxyPlot
open OxyPlot.Series
open OxyPlot.Axes

type Measurement = {Name: string; Unit : string; Scaling : double list}
type Log = {Measurements : Measurement list; Values : seq<float list>}

// Read a delimiter separated values text file
let dsvreader (separator:char) filename =
    let lines = File.ReadLines(filename)
    seq { for value in lines do yield value.Split(separator) }


let meastostring (meas:Measurement) =
    meas.Name + " ["+meas.Unit+"]"
let stringtomeas str =
    let r = Regex.Match(str, @"(.+) ?\[(.+)\]"); // parse "Name [Unit]"
    {Name=r.Groups.[1].Value.Trim();Unit=r.Groups.[2].Value.Trim(); Scaling=[0.0;1.0]}
// Parse a logfile where first line contains a list of measurement names and units and the
// rest contains corresponding measurementvalues
let logfilereader separator filename =
    let lines = dsvreader separator filename
    let meas = List.map stringtomeas (Seq.head lines |> Seq.toList)  
    //let vals = Seq.map (List.map System.Double.Parse) (Seq.tail (Array.toList lines))
    let vals = Seq.map (fun u -> Array.toList (Array.map System.Double.Parse u)) (Seq.tail lines)
    {Measurements=meas; Values=vals}

let rec prng a c m (seed:uint32) = seq {
    let next = (seed * c + a) % m;
    yield next
    yield! prng a c m next
    }


let createWindow title (log:Log) =
    let plotline (series:LineSeries list) line =
        let rec helper (series:LineSeries list) line k t = 
            if List.isEmpty line then () else 
                series.[k].Points.Add(new DataPoint(t, List.head line))
                helper series (List.tail line) (k+1) t
        helper series (List.tail line) 0 (List.head line)
    let myModel = new PlotModel()
    myModel.Axes.Add(new LinearAxis ( Position = AxisPosition.Bottom, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Tid" ))
    myModel.Axes.Add(new LinearAxis ( Position = AxisPosition.Left, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Verdi" ))
    let series = List.map (fun x -> new LineSeries(Title=(meastostring x), StrokeThickness=4.0)) (List.tail log.Measurements)
    for serie in series do myModel.Series.Add(serie)
    for line in log.Values do plotline series line
    let plot1 = new PlotView(Dock=DockStyle.Fill, BackColor=Color.White, Model=myModel)
    let menuItem = new MenuItem("File")
    let menuBar = new MainMenu(items=[|menuItem|])
    let form = new Form(Text=title, Menu=menuBar);
    form.Controls.Add(plot1)
    form

type IView =
    abstract member Setup : Measurements : Measurement list -> unit
    abstract member Plot  :  Values : seq<float list> -> unit
    abstract member Show : unit -> unit

type FormView(title) =
    let plot1 = new PlotView(Dock=DockStyle.Fill, BackColor=Color.White)
    let menuItem = new MenuItem("File")
    let menuBar = new MainMenu(items=[|menuItem|])
    let form = new Form(Text=title, Menu=menuBar);
    do form.Controls.Add(plot1);
    let mutable series : LineSeries list = []
    let plotline (series:LineSeries list) line =
        let rec helper (series:LineSeries list) line k t = 
            if List.isEmpty line then () else 
                series.[k].Points.Add(new DataPoint(t, List.head line))
                helper series (List.tail line) (k+1) t
        helper series (List.tail line) 0 (List.head line)
    interface IView with
        member x.Setup meas =
            let model = new PlotModel()
            model.Axes.Add(new LinearAxis ( Position = AxisPosition.Bottom, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Tid" ))
            model.Axes.Add(new LinearAxis ( Position = AxisPosition.Left, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Verdi" ))
            series <- List.map (fun x -> new LineSeries(Title=(meastostring x), StrokeThickness=4.0)) (List.tail meas)
            for serie in series do model.Series.Add(serie)
            plot1.Model <- model
        member x.Plot values =
            for line in values do plotline series line
        member x.Show() =
            form.ShowDialog() |> ignore
[<EntryPoint>]
let main argv = 
    //let log = logfilereader '\t' @"E:\Documents\logg2.tsv"
    let log = {Measurements=List.map stringtomeas ["Time [sec]"; "Temp [C]"; "Pressure [psi]";"Sum [ ]"];Values=seq{for i in 0.0..0.1..10.0 do yield [float(i);float(i*i);100.0-10.0*float(i);float(i*i)+(100.0-10.0*float(i))]}}
    let win = new FormView("Plott") :> IView;
    win.Setup log.Measurements
    win.Plot log.Values
    //let window = createWindow "Plott" {Measurements=List.map stringtomeas ["Time [sec]"; "Temp [C]"; "Pressure [psi]";"Sum [ ]"];Values=seq{for i in 0.0..0.1..10.0 do yield [float(i);float(i*i);100.0-10.0*float(i);float(i*i)+(100.0-10.0*float(i))]}}
    //printfn "%A" log
   // List.map (String.concat ' ') (Seq.toList log.Values) |> ignore
    //(Seq.toList log.Values) |> List.map (fun x -> printfn "%A %A" x.[0] x.[1]) |> ignore
    //printfn "%A" (prng 1103515245u 12345u 4294967295u 0u)
    win.Show()
    //window.ShowDialog() |> ignore
    0 // return an integer exit code
